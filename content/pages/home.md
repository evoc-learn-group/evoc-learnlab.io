title: evoc-learn
author: gerazov
save_as: index.html
url: 


# The evoc-learn Project

We test the hypothesis that children learn by using their own articulators as a learning device to experiment with different vocal manoeuvres until they sound sufficiently like adults. 

To this end we rely on state-of-the-art articulatory synthesis to emulate all the critical elements of the learning process, with the goal to generate highly accurate and natural sounding speech with the learned articulatory parameters. 

# News

- Our Show&Tell proposal got accepted for the upcoming Interspeech 2022.
- We recently submitted three papers to the upcoming Interspeech 2022.
- Project meeting scheduled in Macedonia in June 2022.


