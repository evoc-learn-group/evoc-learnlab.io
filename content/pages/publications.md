title: evoc-learn Publications
author: gerazov
save_as: publications.html
url: publications


# Publications

- Krug P.K., B. Gerazov, D.R. van Niekerk, A. Xu, Y. Xu, and P. Birkholz, “Modelling microprosodic effects can lead to an audible improvement in articulatory synthesis,” Journal of the Acoustical Society of America, vol. 150 (2), pp. 1209 – 1217, Aug 2021. \[[pdf](http://www.homepages.ucl.ac.uk/~uclyyix/yispapers/Krug_etAl_JASA2021.pdf)\]

- Xu A., D. van Niekerk, B. Gerazov, P. K. Krug, S. Prom-on, P. Birkholz, and Y. Xu, “Model-based exploration of linking between vowel articulatory space and acoustic space,” Interspeech, Brno, Czechia, 30 Aug – 3 Sep 2021.
\[[pdf](https://discovery.ucl.ac.uk/id/eprint/10139361/1/xu21j_interspeech.pdf)\]

- Van Niekerk D.R., A. Xu, B. Gerazov, P.K. Krug, P. Birkholz, Y. Xu, “Finding intelligible consonant-vowel sounds using high-quality articulatory synthesis,” Interspeech, Shanghai, China, 25 – 29 Oct 2020.
\[[pdf](https://discovery.ucl.ac.uk/id/eprint/10118763/1/vanNiekerk_Interspeech2020.pdf)\]

# In the pipeline

- Gerazov B., D. van Niekerk, A. Xu, P.K. Krug, P. Birkholz, and Y. Xu, “Evaluating Features and Metrics for High-Quality Simulation of Early Vocal Learning of Vowels,” in ArXiv e-prints, 2 Apr 2021. 
\[[pdf](https://arxiv.org/abs/2005.09986)\]

